const Discord = require("discord.js");
const errors = require("../utils/errors.js");

module.exports.run = async (bot, message, args) => {
  let rUser = message.guild.member(message.mentions.users.first() || message.guild.member.get(args[0]));
    if(!rUser) return message.channel.send("Impossible de trouver l'utilisateur.");
    let reason = args.join(" ").slice(22);

    let reportEmbed = new Discord.RichEmbed()
    .setDescription("Rapports")
    .setColor("#15f153")
    .addField("Utilisateur Signalé", `${rUser} with ID: ${rUser.id}`)
    .addField("Signalé Par", `${message.author} with ID: ${message.author.id}`)
    .addField("Canal", message.channel)
    .addField("Temps", message.createdAt)
    .addField("Raison", reason);

    let reportschannel = message.guild.channels.find(`id`, "510868728720916490");
    if(!reportschannel) return message.channel.send("Impossible de trouver le canal des rapports.");

    reportschannel.send(reportEmbed);

}

module.exports.help = {
  name: "report"
}
