const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let bicon = bot.user.displayAvatarURL;
    let botembed = new Discord.RichEmbed()
    .setTitle("Bot Info")
    .setDescription("Ce bot a été fait par @● Pablo Escobars ●#0001 aka AidaForceYT")
    .setColor("#15f153")
    .setThumbnail(bicon)
    .addField("Nom du Bot", bot.user.username)

    message.channel.send(botembed);
}

module.exports.help = {
  name:"botinfo"
}
