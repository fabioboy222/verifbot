const Discord = require("discord.js");
let tokens = require("./tokens.json");

module.exports.run = async (bot, message, args) => {
  if(!tokens[message.author.id]){
    tokens[message.author.id] = {
      tokens: 50
    };
  }

  let uTokens = tokens[message.author.id].tokens;


  let tokenEmbed = new Discord.RichEmbed()
  .setAuthor(message.author.username)
  .setColor("#00FF00")
  .addField("💸", uTokens);

  message.channel.send(tokenEmbed).then(msg => {msg.delete(5000)});

}

module.exports.help = {
  name: "tokens"
}
