const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {

  if(!args[2]) return message.reply("S'il vous plaît poser une question!");
  let replies = ["Oui.", "Non.", "Je sais pas.", "Demander à nouveau plus tard."];

  let result = Math.floor((Math.random() * replies.length));
  let question = args.slice(1).join(" ");

  let ballembed = new Discord.RichEmbed()
  .setAuthor(message.author.tag)
  .setColor("#FF9900")
  .addField("Question", question)
  .addField("Réponse", replies[result]);

  message.channel.send(ballembed);
}

module.exports.help = {
  name: "8ball"
}
