const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let sicon = message.guild.iconURL;
    let serverembed = new Discord.RichEmbed()
    .setTitle("Informations sur le Serveur")
    .setDescription("Ce serveur a été créé par @● Pablo Escobars ●#0001, Pour le serveur garry's mod [FR] CristsRP.")
    .setColor("#15f153")
    .setThumbnail(sicon)
    .addField("Nom du Serveur", message.guild.name)
    .addField("Nombre total de Membres", message.guild.memberCount);

    message.channel.send(serverembed);
}

module.exports.help = {
  name:"serverinfo"
}
