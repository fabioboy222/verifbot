const Discord = require("discord.js");
const fs = require("fs");
let tokens = require("../tokens.json");

module.exports.run = async (bot, message, args) => {
  //!pay @isatisfied 59345

  if(!tokens[message.author.id]){
    return message.reply("Vous n'avez pas de Tokens!")
  }

  let pUser = message.guild.member(message.mentions.users.first()) || message.guild.members.get(args[0]);

  if(!tokens[pUser.id]){
    tokens[pUser.id] = {
      tokens: 0
    };
  }

  let pCoins = coins[pUser.id].tokens;
  let sCoins = tokens[message.author.id].tokens;

  if(sCoins < args[0]) return message.reply("Pas assez de tokens!");

  tokens[message.author.id] = {
    tokens: sCoins - parseInt(args[1])
  };

  tokens[pUser.id] = {
    tokens: pCoins + parseInt(args[1])
  };

  message.channel.send(`${message.author} a donné ${pUser} ${args[1]} tokens.`);

  fs.writeFile("./tokens.json", JSON.stringify(tokens), (err) => {
    if(err) cosole.log(err)
  });


}

module.exports.help = {
  name: "pay"
}
