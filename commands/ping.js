const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {

  let pingem = new Discord.RichEmbed()
  .setDescription(`Pong! ${Math.floor(bot.ping)}ms. 🏓`)
  .setColor("#00FA9A")

  message.channel.send(pingem);
}

module.exports.help = {
  name: "ping"
}
